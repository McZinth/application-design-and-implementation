package assignment;

/**
 * @author Christopher Hyde
 * @dueDate Novemenber 25, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class is a servlet test for controlling user entered parameters.
 */

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Initialization Method
	 */
    @Override
	public void init(ServletConfig config) throws ServletException {
		//Print Test Message
    	System.out.println("LOG: TestServlet Initialized");
	}

	/**
	 * Destroy Method
	 */
    @Override
	public void destroy() {
    	//Print Test Message
    	System.out.println("LOG: TestServlet Destroyed");
	}

	/**
	 * Default doGet method
	 * @param HttpServletRequest request, HttpServletResponse response
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		
		//Test if attributes meet criteria
		if( firstName != null && !firstName.isEmpty() && lastName != null && !lastName.isEmpty() ) {
			//set the attributes of this servlet to the parameters. 
			request.setAttribute("firstName", request.getParameter("firstName"));
			request.setAttribute("lastName", request.getParameter("lastName"));
			//Forward to TestResponse.jsp
			request.getRequestDispatcher("TestResponse.jsp").forward(request, response);
			
		}else {
			//redirect to TestError.jsp
			response.sendRedirect("TestError.jsp");
			
		}
		
		

	}
}
