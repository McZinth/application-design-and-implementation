package controllers;

/**
 * @author Christopher Hyde
 * @dueDate Novemenber 25, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class is the JSF Managed bean controlling the form for
 * 			assignment2b.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import beans.User;

@ManagedBean( name = "FormController", eager = true)
@RequestScoped
public class FormController {

	/**
	 * Method for controlling the Submit commandButton on TestForm.xhtml
	 * @param user - User Managed Bean
	 * @return String - url
	 */
	public String onSubmit(User user) {
		
		//Set the 'user' attribute to FacesContext request Map
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("User",user);		
		return "TestResponse";
	}
	
	/**
	 * Method for controlling the Flash commandButton on TestForm.xhtml
	 * @param user - User Managed Bean
	 * @return String - url with redirect query
	 */
	public String onFlash(User user) {
		
		//Set the 'user' attribute to FacesContext request Map
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("User", user);	
		return "TestResponse2.xhtml?faces-redirect=true";
	}
	
	
}
