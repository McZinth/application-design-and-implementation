package beans;

/**
 * @author Christopher Hyde
 * @dueDate Novemenber 25, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class is the JSF Managed bean controlling the user properties for
 * 			assignment2b.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean( name = "User", eager = true)
@RequestScoped
public class User {
	
	//Properties
	private String firstName;
	private String lastName;
	
	/**
	 * Default Constructor
	 */
	public User() {
		//Set properties default value
		this.firstName = "Chris";
		this.lastName = "Hyde";
	}
	
	/**
	 * Getter for firstName property
	 * @return String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter for firstName property
	 * @param String
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter for lastName property
	 * @return String
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter for lastName property
	 * @param String
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
	

}
